package ejercicio1;
public class stackDinamic {
 int arr[];
    int top; 
    stackDinamic(){
        this.arr = new int[3];
        this.top = -1;
    }  
    public boolean isEmpty(){
        return (top == -1);
    }
    
    public void push(int element){
        top++;//0
        if(top < arr.length){//4 < 3
            arr[top] = element;
        }else{
            int temp[] = new int[arr.length+2];//3+2=5
            for(int i=0; i<arr.length;i++){
                temp[i] = arr[i];
            }
            arr = temp;
            arr[top] = element;
        }
    }
    public int pop(){
        if(!isEmpty()){ // ! isEmpty() = true
            int returntop = top;
            top--; 
            int temp = arr[returntop];
            return temp;
        }else{
            System.out.println("la pila ya esta vacia");
            return -1;
        }
    }
    public int peek(){
        return arr[top];
    }
}
