package ejercicio1;
import javax.swing.JOptionPane;
/*Realizar un programa que pida números por pantalla y sean almacenados en una 
pila (estática o dinámica) y crear una función para que la pila sea ordenada de
manera ascendente y descendente pueden usar pilas auxiliares si es necesario 
         1.- ENTRADA 7,3,5,4,6,7,8 SALIDA 3,4,5,6,7,7,8 
         2.- ENTRADA 7,3,5,4,6,7,8 SALIDA 8,7,7,6,5,4,3 
-El programa debe solicitar que tipo de pila se desea utilizar para el almacenamiento
-Las pilas deben ser creadas por usted y no así el uso de la clase stack de java. 
-Los resultados deben ser mostrados por pantalla como lo vamos haciendo en clases 
-RESPETAR LA ESTRUCTURA DE DATOS DE LA PILA EL PRIMERO EN ENTRAR DEBE SER EL
ULTIMO EN SALIR Y EL ULTIMO EN ENTRAR DEBE SER EL PRIMER EN SALIR
*/
public class ejercicio1 {
    public static void main(String[] args) {
      String inputText,inputSelect,inputData,inputType;
        inputText = JOptionPane.showInputDialog("introdusca el numero de datos que desea ingresar");
        inputSelect = JOptionPane.showInputDialog("Que tipo de pila desea utilizar: \n -dinamica \n -estatica");
        int con = 1;
        int limit = Integer.parseInt(inputText);
        String result = "";
        switch(inputSelect){
            case "dinamica":
                stackDinamic p = new stackDinamic();
                do{
                    inputData = JOptionPane.showInputDialog("introduzca un numero entero");
                    if(inputData.length() == 0){
              inputData = JOptionPane.showInputDialog("introduzca un numero entero");    
                    }else{
                        p.push(Integer.parseInt(inputData));
                    }
                    con++;
                }while(con <= limit);
                inputType = JOptionPane.showInputDialog("ordenar: \n -ascendente \n -desscendente");
                stackDinamic resultStackDinamic = sortStackDinamic(p,inputType);
                while(!resultStackDinamic.isEmpty()){
                    result = result+ " - " + resultStackDinamic.pop();
                }
                JOptionPane.showMessageDialog(null, result);
            break;
            case "estatica":
                stackStatick ps = new stackStatick(limit);
                do{
                    inputData = JOptionPane.showInputDialog("introduzca un numero entero");
                    if(inputData.length() == 0){
                        inputData = JOptionPane.showInputDialog("introduzca un numero entero");    
                    }else{
                        ps.push(Integer.parseInt(inputData));
                    }
                    con++;
                }while(con <= limit);
                inputType = JOptionPane.showInputDialog("ordenar: \n -ascendente \n -descendente");
                stackStatick resultStackStatic = sortStackStatick(ps,inputType);
                while(!resultStackStatic.isEmpty()){
                    result = result+ " - " + resultStackStatic.pop();
                }    
                JOptionPane.showMessageDialog(null, result);
            break;
            default:
                JOptionPane.showMessageDialog(null, "elija una opcion valida");
            break;
        }
    }
    
    public static stackStatick sortStackStatick(stackStatick p,String type){
        stackStatick tp = new stackStatick(10);
        while(!p.isEmpty()){
            int currentData = p.pop();
            if(type == "ascendente"){
                while(!tp.isEmpty() && tp.peek() > currentData){
                    p.push(tp.pop());
                }
                tp.push(currentData);
            }else{
                while(!tp.isEmpty() && tp.peek() < currentData){
                    p.push(tp.pop());
                }
                tp.push(currentData);        
            }
        }
        return tp;
    }
    
    public static stackDinamic sortStackDinamic(stackDinamic p,String type){
        stackDinamic tp = new stackDinamic();
        while(!p.isEmpty()){
            int currentData = p.pop();
            while(!tp.isEmpty() && tp.peek() > currentData){
                p.push(tp.pop());
            }
            tp.push(currentData);
        }
        return tp;
    }
}
