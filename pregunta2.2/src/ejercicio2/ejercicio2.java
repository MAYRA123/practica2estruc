package ejercicio2;
import javax.swing.JOptionPane;
/*Realizar un programa que pida números por pantalla y sean almacenados en una pila (estática o dinámica) y crear una función para que no se muestren los números repetidos que están almacenados el resultado final debe ser ordenado de manera ascendente pueden usar pilas auxiliares si es necesario 
1.- ENTRADA 7,3,4,4,6,7,8 SALIDA 3,6,8 
-El programa debe solicitar que tipo de pila se desea utilizar para el almacenamiento 
-Las pilas deben ser creadas por usted y no así el uso de la clase stack de java. 
-Los resultados deben ser mostrados por pantalla como lo vamos haciendo en clases 
-RESPETAR LA ESTRUCTURA DE DATOS DE LA PILA EL PRIMERO EN ENTRAR DEBE SER EL 
ULTIMO EN SALIR Y EL ULTIMO EN ENTRAR DEBE SER EL PRIMER EN SALIR */
public class ejercicio2 {
   public static void  main(String[] args){
        String inputlimit,inputtype,inputdata;
        inputlimit = JOptionPane.showInputDialog("Introduzca un limite de datos");
        inputtype = JOptionPane.showInputDialog("Que pila desea usar :\n -estatica \n -dinamica");
        int auxil = 1;
        int lim = Integer.parseInt(inputlimit);
        String rest = "";
        switch(inputtype){
            case "dinamica":
                stackDinamic pd = new stackDinamic();
                do{
                    inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    if(inputdata.length() == 0)
                        inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    else
                        pd.push(Integer.parseInt(inputdata));
                    auxil++;
                }while(auxil <= lim);
                stackDinamic restpd = dinamicNumbers(pd);
                while(!restpd.isEmpty()){
                    rest = rest +'-'+restpd.pop();
                }
                JOptionPane.showMessageDialog(null, "resultado de pila dinamica:\n"+rest);
            break;
            case "estatica":
                stackStatic ps = new stackStatic(lim);
                do{
                    inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    if(inputdata.length() == 0)
                        inputdata = JOptionPane.showInputDialog("Introduzca un numero entero");
                    else
                        ps.push(Integer.parseInt(inputdata));
                    auxil++;
                }while(auxil <= lim);
                stackStatic resultps = stackNumbers(ps,lim);
                while(!resultps.isEmpty()){
                    rest = rest +'-'+resultps.pop();
                }
                JOptionPane.showMessageDialog(null, "resultado de pila estatica:\n"+rest);
            break;
            default:
                JOptionPane.showMessageDialog(null, "Escoja un valor valido");
            break;  
        }
    }
    public static stackDinamic dinamicNumbers(stackDinamic di){
            stackDinamic di1 = new stackDinamic();
            stackDinamic di2 = new stackDinamic();
            while(!di.isEmpty()){       
                int temp = di.pop();
                int count = 1;        
                while(!di.isEmpty()){
                    int temp2 = di.pop();  
                    if(temp2 == temp){
                        count++; 
                    }else{
                        di2.push(temp2);
                    }
                }
                if(count == 1){
                    di1.push(temp);
                }
                while(!di2.isEmpty()){
                        di.push(di2.pop());
                }
            }
            return di1;
    }
    public static stackStatic stackNumbers(stackStatic sta, int lim){
        stackStatic sta1 = new stackStatic(lim);
        stackStatic sta2 = new stackStatic(lim);
        while(!sta.isEmpty()){      
            int temp = sta.pop();
            int currentData= sta.pop();
             while(!sta1.isEmpty() && sta.peek() < currentData){
                    sta.push(sta1.pop());
                }
                sta1.push(currentData);
            int count = 1; 
            while(!sta.isEmpty()){
                int temp2 = sta.pop();
                if(temp2 == temp){
                    count++; 
                }else{
                    sta2.push(temp2);
                }
            }
            if(count == 1){
                sta1.push(temp);
            }
            while(!sta2.isEmpty()){
                sta.push(sta2.pop());
            }
        }
        return sta1;
    }   
}
