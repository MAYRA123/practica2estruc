package ejercicio2;
public class stackStatic {
int size; 
    int arr[];
    int top;
    stackStatic(int size){
        this.size = size;
        this.arr = new int[size];
        this.top = -1;
    }
    public boolean isEmpty(){
        return (top == -1);
    }
    public void push(int element){
           if(top<size-1){
               top++;
               arr[top] = element;
           }else{
               System.out.print("la pila esta llena");
           }
    }
    public int peek(){
        return arr[top];
    }
    public int pop(){
        if(!isEmpty()){
            int temptop = top;
            top--;
            int returndata = arr[temptop];
            return returndata;
        }else{
            System.out.print("la pila esta vacia");
            return -1;
        }
    }
    
}
