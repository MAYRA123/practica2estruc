package ejercicio3;
import javax.swing.JOptionPane;
/**
 Realizar un programa que pida números por pantalla y sean almacenados en una pila (estática o dinámica) y crear una función para que los números se
 * muestren una sola vez el resultado final debe ser ordenado de manera descendente pueden usar pilas auxiliares si es necesario 
1.- ENTRADA 7,3,4,4,6,7,8 SALIDA 8,7,6,4,3 
-El programa debe solicitar que tipo de pila se desea utilizar para el almacenamiento 
-Las pilas deben ser creadas por usted y no así el uso de la clase stack de java. 
-Los resultados deben ser mostrados por pantalla como lo vamos haciendo en clases 
-RESPETAR LA ESTRUCTURA DE DATOS DE LA PILA EL PRIMERO EN ENTRAR DEBE SER EL ULTIMO EN SALIR Y EL ULTIMO EN ENTRAR DEBE SER EL PRIMER EN SALIR 
 */
public class ejercicio3 {
public static void main(String[] args) {
      String inputText,inputSelect,inputData;
        inputText = JOptionPane.showInputDialog("introdusca el numero de datos que desea ingresar");
        inputSelect = JOptionPane.showInputDialog("Que tipo de pila desea utilizar: \n -dinamica \n -estatica");
        int c = 1;
        int l = Integer.parseInt(inputText);
        String res = "";
        switch(inputSelect){
            case "dinamica":
                stackDinamic p1 = new stackDinamic();
                do{
                    inputData = JOptionPane.showInputDialog("introduzca un numero entero");
                    if(inputData.length() == 0){
              inputData = JOptionPane.showInputDialog("introduzca un numero entero");    
                    }else{
                        p1.push(Integer.parseInt(inputData));
                    }
                    c++;
                }while(c <= l);
                stackDinamic resultStackDinamic = sortStackDinamic(p1);
                while(!resultStackDinamic.isEmpty()){
                    res = res+ " - " + resultStackDinamic.pop();
                }
                JOptionPane.showMessageDialog(null, res);
            break;
            case "estatica":
                stackStatick p2 = new stackStatick(l);
                do{
                    inputData = JOptionPane.showInputDialog("introduzca un numero entero");
                    if(inputData.length() == 0){
                        inputData = JOptionPane.showInputDialog("introduzca un numero entero");    
                    }else{
                        p2.push(Integer.parseInt(inputData));
                    }
                    c++;
                }while(c <= l);
                stackStatick resultStackStatic = sortStackStatick(p2);
                while(!resultStackStatic.isEmpty()){
                    res = res+ " - " + resultStackStatic.pop();
                }    
                JOptionPane.showMessageDialog(null, res);
            break;
            default:
                JOptionPane.showMessageDialog(null, "elija una opcion valida");
            break;
        }
    }
    public static stackStatick sortStackStatick(stackStatick pst){
        stackStatick sta = new stackStatick(10);
        while(!pst.isEmpty()){
            int currentData = pst.pop();
                while(!sta.isEmpty() && sta.peek() > currentData){
                    pst.push(sta.pop());
                }
                sta.push(currentData);       
            }
        return sta;
    }
    public static stackDinamic sortStackDinamic(stackDinamic pdi){
        stackDinamic din = new stackDinamic();
        while(!pdi.isEmpty()){
            int currentData = pdi.pop();
            {while(!din.isEmpty() && din.peek() > currentData){
                pdi.push(din.pop());
              }  
            }
            din.push(currentData); 
        }
        return din;
    }
}
