package ejercicio3;
public class stackStatick {    
int size;
    int arr[];
    int top;
    
    stackStatick(int size){
        this.size = size;
        this.arr = new int[size];
        this.top = -1;
    }
    
    public boolean isFull(){
        return (size-1 == top);
    }
    
    public boolean isEmpty(){
        return (top == -1);
    }
    
    public void push(int element){
       if(!isFull()){
           top++;
           arr[top] = element;
       }else{
           System.out.println("la pila esta llena");
       }
    }
    
    public int pop(){
        if(!isEmpty()){
            int returntop = top;
            top--;
            int temp = arr[returntop];
            return temp;
        }
        else{
            System.out.println("la pila esta vacia");
            return -1;
        }
    }
    
    public int peek(){
        return arr[top];
    }
}
